#!/bin/bash

prediction_url = "https://southcentralus.api.cognitive.microsoft.com/customvision/v2.0/Prediction/ca330c7f-438a-4588-b0c2-b5bd4b7c44e5/image"
prediction_key = "99b4118d538b407a9604707015225c32"
content_type = "application/octet-stream"
first_left = FirstBrushOK1L.png
first_right = FirstBrushOK1R.png
second_left = SecondBrushOK1L.png
second_right = SecondBrushOK1R.png


for path in ./data/*; do
    [ -d "${path}" ] || continue # if not a directory, skip
    dirname="$(basename "${path}")"
    curl -k -H "Prediction-Key: 99b4118d538b407a9604707015225c32" -H "Content-Type: application/octet-stream" --data-binary "@data/$dirname/$first_left" $prediction_url > first_left.json
    curl -k -H "Prediction-Key: 99b4118d538b407a9604707015225c32" -H "Content-Type: application/octet-stream" --data-binary "@data/$dirname/$second_left" $prediction_url > second_left.json
done

